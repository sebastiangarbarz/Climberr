using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Sprite spriteRight = null;
    [SerializeField] Sprite spriteLeft = null;
    [SerializeField] Sprite spriteUp= null;
    [SerializeField] GameManager gameManager = null;
    private float stamina, fear = 0f;
    private float health = 1f;
    
    private SpriteRenderer spriteRenderer = null;
    private CharacterController charController = null;
    private CapsuleCollider2D capsuleCollider2D = null;
    private GameObject lastGrab = null;
    public float Stamina { get => stamina; set => stamina = value; }
    public float Fear { get => fear; set => fear = value; }
    public float Health { get => health; set => health = value; }

    // Start is called before the first frame update
    void Awake()
    {
        GameObject go = new GameObject();
        lastGrab = go;
        spriteRenderer = GetComponent<SpriteRenderer>();
        charController = GetComponent<CharacterController>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeSprite(string direction) {
        
        if (direction == "Right") 
        {
            spriteRenderer.sprite = spriteRight;    
        }
        if (direction == "Left")
        {
            spriteRenderer.sprite = spriteLeft;
        }
        if (direction == "Up")
        {
            spriteRenderer.sprite = spriteUp;
        }

    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I grabbed: " + collision.gameObject.name);
        Debug.Log("Last grab is: " + lastGrab);
        Debug.Log("Entered level " + gameManager.Entered_level);
        if (!lastGrab) {
            GameObject z = new GameObject();
            lastGrab = z;
        }
        
        if (collision.CompareTag("Grab") && (lastGrab.name != collision.gameObject.name || gameManager.Entered_level))  /* Sino puedo colisionar contra el mismo grab dos veces. */
        {
            Debug.Log("que onda colision");
            charController.Move1 = false;
            gameManager.Grabbed = true;
            gameManager.Number_Of_Climbs1 = 0; /* Si se agrr*/
            gameManager.Entered_level = false;
            gameManager.Slider.value = Mathf.Lerp(gameManager.Slider.value, 1f, 1.2f*Time.deltaTime);

        }
        if (collision.CompareTag("Bandera"))
        {
            charController.Move1 = false;
            gameManager.NextLevel();
            
        }
        if (collision.CompareTag("Boundary")) {
            Debug.Log("Boundary "+gameManager.Number_Of_Climbs1);
            charController.Move1 = false;
            if (gameManager.Number_Of_Climbs1 == 2) { /* Uso sus dos escaladas y llego a una pared, cae.*/
                Debug.Log("Will set falls to true "+collision.name);
                charController.Falls = true;
            }
            
        }
        lastGrab = collision.gameObject;


    }
    private void OnCollisionEnter2D(Collision2D collision) /* Sin uso.*/
    {
        Debug.Log("Collision! ");
        ContactPoint2D c= collision.GetContact(0);
        Vector3 a = c.point; //this is the Vector3 position of the point of contact
        Debug.Log("Collision;: " + a);
    }
     
}

