﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI txt_CenterTxt = null;
    [SerializeField] TextMeshProUGUI txt_number_of_climbs = null;
    [SerializeField] CharacterController charController = null;
    [SerializeField] Player player = null;
    [SerializeField] GameObject IceGrab = null;
    [SerializeField] AudioClip audio_menu = null;
    [SerializeField] AudioClip audio_jump = null;
    [SerializeField] Slider slider = null;
    [SerializeField] float sliderSpeed = 0.1f;

    private int Number_Of_Climbs = 0;
    private GameObject prefabGrab = null;

    private Vector3 Player_Original_Position;
    private bool grabbed = false;
    private bool entered_level = false;
    private string Waiting_Status = "";
    private AudioSource audioSource;


    public int Number_Of_Climbs1 { get => Number_Of_Climbs; set => Number_Of_Climbs = value; }
    public bool Grabbed { get => grabbed; set => grabbed = value; }
    public AudioSource AudioSource { get => audioSource; set => audioSource = value; }
    public bool Entered_level { get => entered_level; set => entered_level = value; }
    public Slider Slider { get => slider; set => slider = value; }

    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
        AudioSource.Play(0); /* Menu */
    }
    private void Start()
    {
        Player_Original_Position = player.transform.position;
    }
    private void Update()
    {
        txt_number_of_climbs.text = Number_Of_Climbs.ToString();
        if (grabbed)
        {
            Slider.value = Mathf.Lerp(Slider.value, 0f, 0.1f * sliderSpeed * Time.deltaTime);
        }
        else
        {
            Slider.value = Mathf.Lerp(Slider.value, 0f, sliderSpeed * Time.deltaTime);
        }
        
    }
    #region Spawns
    public void SpawnGrabs(int i)
    {
        if (i == 1)
        {  /* Primer grab */
            float ejeX = Randomize(-1.11f, 3.62f);
            GameObject new_Ice_Grab = Instantiate(IceGrab, new Vector3(ejeX, -2.57f, 1f), Quaternion.identity);
            new_Ice_Grab.name = "Grab(Clone)_" + i.ToString();
        }
        if (i == 2)
        { /* Second grab */
            float ejeX = Randomize(-0.19f, 3.35f);
            GameObject new_Ice_Grab = Instantiate(IceGrab, new Vector3(ejeX, -1.49f, 1f), Quaternion.identity);
            new_Ice_Grab.name = "Grab(Clone)_" + i.ToString();
        }
        if (i == 3) /* Third grab */
        {
            float ejeX = Randomize(0.89f, 3.15f);
            GameObject new_Ice_Grab = Instantiate(IceGrab, new Vector3(ejeX, -0.21f, 1f), Quaternion.identity);
            new_Ice_Grab.name = "Grab(Clone)_" + i.ToString();
        }
        if (i == 4) /* Fourth grab */
        {
            float ejeX = Randomize(-0.45f, 2.56f);
            GameObject new_Ice_Grab = Instantiate(IceGrab, new Vector3(ejeX, 1.34f, 1f), Quaternion.identity);
            new_Ice_Grab.name = "Grab(Clone)_" + i.ToString();
        }
        if (i == 5) /* Fifth grab */
        {
            float ejeX = Randomize(-0.2f, 2.13f);
            GameObject new_Ice_Grab = Instantiate(IceGrab, new Vector3(ejeX, 3.03f, 1f), Quaternion.identity);
            new_Ice_Grab.name = "Grab(Clone)_" + i.ToString();
        }



    }
    public float Randomize(float desde, float hasta)
    {
        return Random.Range(desde, hasta);
    }
    public float RandomY()
    {
        return 1f;
    }
    public Transform GivePosition_ToGrab()
    {
        return transform;
    }
    #endregion
    #region Routines
    public IEnumerator PlayerWillFallAfter(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        if (!Grabbed && Number_Of_Climbs1 >= 2)
        {
            Debug.Log("Falls");
            charController.Falls = true;
            Slider.value = Mathf.Lerp(Slider.value, 0f, 2*Time.deltaTime);
        }



    }
    public IEnumerator WaitForClimberToGrab()
    {
        yield return new WaitForSeconds(2.75f); /* Que la espera sea del nuevo grab */
        if (Vector3.Distance(player.transform.position, charController.Target) < 0.1f)
        {
            Debug.Log("Me puedo caer porque llegué.");
        }

        if (Number_Of_Climbs == 2)
        {
            if (!Grabbed && Waiting_Status != "Waiting") /* Chequeo que no esté ya esperando un grab */
            { /* No se agarró, se cae */
                Waiting_Status = "Waiting";

                if (!Grabbed && Number_Of_Climbs >= 2 && Waiting_Status == "Waiting")
                { /* Espero a que se agrre y aun asi no se agarro, y ya escalo 2 veces */
                    Debug.Log(" Grabbed: " + Grabbed + " Number of climbs:  " + Number_Of_Climbs + "Me caigo, Climbs 2 and falls will be true ");
                    charController.Falls = true;
                    yield return null;
                }
            }
            else
            {
                Waiting_Status = "";
                Slider.value = Mathf.Lerp(Slider.value, 1f, Time.deltaTime);
                Number_Of_Climbs = 0; /* Puede seguir escalando */
                Debug.Log("Player is grabbed after two climbs, resetting number of climbs to zero " + Number_Of_Climbs);
                yield return null;
            }
        }
        else
        if (Number_Of_Climbs == 1) /* No debería entrar nunca */
        {

            Waiting_Status = "";
            Debug.Log(" No debería entrar nunca  Escale uan vez, no pasa nada");/* */
        }

    }


    public IEnumerator TextRoutine(string what_happened)
    {


        if (what_happened == "Game Over")
        {
            Debug.Log("Game Over!");
            charController.Falls = false; /* que se caiga una vez nomá. */
            txt_CenterTxt.text = "Game Over!";
        }
        if (what_happened == "Level Up")
        {
            txt_CenterTxt.text = "Level Up!";
        }


        Color color = txt_CenterTxt.color;
        while (txt_CenterTxt.color.a < 1f)
        { /* Fade In */
            color.a += Time.deltaTime;
            txt_CenterTxt.fontSize += Time.deltaTime * 5f;
            txt_CenterTxt.color = color;

            yield return null;
        }
        yield return new WaitForSeconds(1.0f);

        while (txt_CenterTxt.color.a > 0.99f) /* Fade Out */
        {

            color.a -= Time.deltaTime;
            txt_CenterTxt.fontSize -= Time.deltaTime * 5f;
            txt_CenterTxt.color = color;
            charController.Move1 = false;
            yield return null;

        }
        if (what_happened == "Game Over")
        {
            StartCoroutine(WaitForPlayerToRestart());
            yield return new WaitForSeconds(1f);
            txt_CenterTxt.text = "Press R to restart";
        }




    }
    public IEnumerator WaitForPlayerToRestart()
    {

        charController.ListenRestartKey = true; /* Me pongo a escuchar el input en el Update*/
        while (!charController.SceneLoaded) /* Espero a que el jugador toque R. */
        {
            yield return null;
        }
        charController.SceneLoaded = false;
        charController.ListenRestartKey = false;
        Number_Of_Climbs = 0;

    }
    #endregion
    #region LevelManager

    public void NextLevel()
    {
        entered_level = true;
        player.transform.position = Player_Original_Position;
        EraseGrabs();
        StartCoroutine(TextRoutine("Level Up"));
        int GrabsToSpawn = 5; /* -1 */  
        for (int i = 1; i <= GrabsToSpawn; i++)
        {
            Debug.Log("Will spawn once");
            SpawnGrabs(i);
        }
        Number_Of_Climbs = 0;
        
    }

    public void EraseGrabs()
    {
        GameObject[] array_of_grabs = GameObject.FindGameObjectsWithTag("Grab");

        for (int i = 0; i < array_of_grabs.Length; i++)
        {
            Destroy(array_of_grabs[i]);
            Debug.Log("Destroyed: " + array_of_grabs[i].name);
        }
    }
    #endregion
}
