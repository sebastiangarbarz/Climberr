using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    #region Private Fields

    [SerializeField] Player player = null;
    [SerializeField] GameManager gameManager = null;
    [SerializeField] SceneController sceneManager = null;
    [SerializeField] Canvas canvasMenu = null;
    [SerializeField] private float climbSpeed = 1.0f; /* A que velocidad se mueve hasta el target*/
    [SerializeField] private float climbStrength = 1.0f; /* Hasta donde voy */
    [SerializeField] private AudioClip ac = null ; /* Hasta donde voy */

    #endregion
    private Rigidbody2D _rigidbody;
    private bool Should_I_Listen_To_Inputs = true;
    private float distance;
    private bool move = false;
    private float verticalMove = 0f;
    private bool m_FacingRight = true;
    private bool listenRestartKey = false;
    private bool sceneLoaded = false;
    private bool falls = false;
    private bool falls_once = true;
    private Vector3 target = new Vector3(0f, 0f, 0f);
    private Vector2 original_position;
    private Vector2 floor;
    public float ClimbStrength { get => climbStrength; set => climbStrength = value; }
    public bool Move1 { get => move; set => move = value; }
    public bool ListenRestartKey { get => listenRestartKey; set => listenRestartKey = value; }
    public bool SceneLoaded { get => sceneLoaded; set => sceneLoaded = value; }
    public bool Falls { get => falls; set => falls = value; }
    public Vector3 Target { get => target; set => target = value; }
    public float Distance { get => distance; set => distance = value; }
    private AudioSource audioSource;


    // Start is called before the first frame update
    void Awake()
    {
        original_position = transform.position;
        _rigidbody = player.GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)){

            if (canvasMenu.isActiveAndEnabled)
            {
                canvasMenu.enabled = false;
                Time.timeScale = 1f;
            }
            else
            {
                canvasMenu.enabled = true;
                Time.timeScale = 0f;
            }
            
            
        }
        if (Should_I_Listen_To_Inputs)
        {
            ListenToInputs();
        }
        if (ListenRestartKey)
        { /* Si tengo que escuchar .*/
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("Aprete R");
                StartCoroutine(sceneManager.LoadScene(0));
                sceneLoaded = true;
                falls_once = true; /* Puedo volver a caerme. */
            }
        }
        CheckClimberPosition();
        if (Move1)
        {
            Move();
        }
        if (Falls)
        {
            PlayerFalls();



        }


    }
    void FixedUpdate()
    {

    }
    public void PlayerFalls()
    {
        if (falls_once)
        { /* Que entre una vez. */
            _rigidbody.gravityScale = 1f;
            floor = new Vector2(player.transform.position.x, -4.2f);
            Target = floor;
            Move1 = true;
            Quaternion quat = new Quaternion(0f, 0f, 180f, 0f);
            transform.rotation = quat;
            Debug.Log("llamo a game over ");
            StartCoroutine(gameManager.TextRoutine("Game Over"));
        }
        falls_once = false;
    }
    public void CheckClimberPosition()
    {


        if (_rigidbody.position == new Vector2(Target.x + 0.01f, Target.y + 0.01f))
        {
            Debug.Log("Player will stop moving");
            Move1 = false;
        }
    }

    public void ListenToInputs()
    {

        if (gameManager.Number_Of_Climbs1 >= 2 && !gameManager.Grabbed && distance < 0.01f)
        { /* No est� agarrado y ya escal� dos veces o m�s. */
            Should_I_Listen_To_Inputs = false;
            {
                Falls = true;
            }
        }
        else
        if (gameManager.Number_Of_Climbs1 < 2)
        {


            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {  /* Player touched UP */
              
                PlayAudio();
                gameManager.Grabbed = false; /* Se empieza a mover, no est� agarrado*/
                Target = new Vector3(_rigidbody.position.x, _rigidbody.position.y + ClimbStrength, 1f);
                gameManager.Number_Of_Climbs1++;
                Move1 = true;
                player.ChangeSprite("Up");
                Debug.Log("UP: Number of climbs: " + gameManager.Number_Of_Climbs1);
                /* if (gameManager.Number_Of_Climbs1 == 2) {
                     StartCoroutine(gameManager.WaitForClimberToGrab());
                 }
                 */
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                PlayAudio();
                gameManager.Grabbed = false; /* Se empieza a mover, no est� agarrado*/
                gameManager.Number_Of_Climbs1++;
                Target = new Vector3(_rigidbody.position.x - 3f, _rigidbody.position.y + ClimbStrength, 1f);
                Move1 = true;
                player.ChangeSprite("Left");
                Debug.Log("Q: Number of climbs: " + gameManager.Number_Of_Climbs1);

                /*if (gameManager.Number_Of_Climbs1 == 2)
                {
                    StartCoroutine(gameManager.WaitForClimberToGrab());
                }*/


            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayAudio();
                gameManager.Grabbed = false; /* Se empieza a mover, no est� agarrado*/
                gameManager.Number_Of_Climbs1++;
                Target = new Vector3(_rigidbody.position.x + 3f, _rigidbody.position.y + ClimbStrength, 1f);
                Move1 = true;
                player.ChangeSprite("Right");
                Debug.Log("E: Number of climbs: " + gameManager.Number_Of_Climbs1);

            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                gameManager.NextLevel();
            }





        }


    }
    private void PlayAudio() {
        Debug.Log("Audio 1: " + audioSource.clip);

        audioSource.Play();
    }

    private void Flip()
    {
        //Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    #region Movement
    public void Move()
    {
        distance = Vector3.Distance(transform.position, target);

        transform.position = Vector3.Lerp(transform.position, Target, climbSpeed * Time.deltaTime);
        //transform.position = new Vector3(transform.position.x, transform.position.y, 1f);


    }
    public void Move_Horizontal(float towardsWhere)
    {

        _rigidbody.velocity += new Vector2(towardsWhere * climbSpeed * Time.deltaTime, 0);

        //animator.SetFloat("speed", _rigidbody.velocity.x);
        // If the input is moving the player right and the player is facing left...
        if (towardsWhere > 0 && !m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (towardsWhere < 0 && m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }

    }


    public void ClimbUp(float moveUp)
    {
        Vector2 oldPosition = _rigidbody.position;
        _rigidbody.MovePosition(new Vector2(oldPosition.x, oldPosition.y + moveUp));

    }
    public void ClimbLeft(float moveLeft)
    {
        Vector2 oldPosition = _rigidbody.position;
        _rigidbody.MovePosition(new Vector2(oldPosition.x, oldPosition.y + moveLeft));
    }
    public void ClimbRight(float moveRight)
    {
        Vector2 oldPosition = _rigidbody.position;
        _rigidbody.MovePosition(new Vector2(oldPosition.x, oldPosition.y + moveRight));
    }

    #endregion

}
