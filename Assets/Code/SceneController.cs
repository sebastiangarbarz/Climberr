using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private SceneController sceneManager = null;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public  IEnumerator LoadScene(int index)
    {
        Debug.Log("WIll load scene");
        AsyncOperation loadingOp = SceneManager.LoadSceneAsync(index);
        while (!loadingOp.isDone)
        {
            
            yield return null;
        }
     

    }
}
